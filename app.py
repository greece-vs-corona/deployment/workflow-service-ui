import ast
import logging
import os
import json
import requests
from flask import Flask, render_template, request, jsonify
from flask_oidc import OpenIDConnect

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
app.config.update({
    'SECRET_KEY': os.environ.get('FLASK_SECRET_KEY'),
    'OIDC_CLIENT_SECRETS': '/etc/oidc_client_secrets/client_secrets.json',
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_SCOPES': ['openid', 'email', 'profile', 'read_api'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_TOKEN_TYPE_HINT': 'access_token',
    'OIDC_OPENID_REALM': os.environ.get('OPENID_REALM')
})

DOWNLOADS_PATH = os.environ.get('DOWNLOADS_PATH')
app.config['OVERWRITE_REDIRECT_URI'] = os.environ.get('OPENID_REALM')

REST_URL = os.environ.get('REST_URL')

oidc = OpenIDConnect(app)


@app.route('/')
@oidc.require_login
def home():
    return render_template("home.html")


@app.route('/submit_workflow', methods=['GET'])
@oidc.require_login
def submit_workflow():
    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return render_template("home.html")

    input_params = extract_workflow_information()
    input_params["access_token"] = access_token

    return render_template("submit_workflow.html", data=input_params)


@app.route('/workflow_metadata')
@oidc.require_login
def workflow_metadata():
    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return render_template("home.html")

    data = {}
    data["access_token"] = access_token
    return render_template("workflow_metadata.html", data=data)


@app.route('/group_workflow_metadata')
@oidc.require_login
def group_workflow_metadata():
    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return render_template("home.html")

    data = {}
    data["access_token"] = access_token
    return render_template("group_workflow_metadata.html", data=data)


@app.route('/inputs')
@oidc.require_login
def inputs():
    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return render_template("home.html")

    data = {}
    data["access_token"] = access_token
    return render_template("inputs.html", data=data)


@app.route('/outputs')
@oidc.require_login
def outputs():
    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return render_template("home.html")

    data = {}
    data["access_token"] = access_token
    return render_template("outputs.html", data=data)


@app.route('/logout')
def logout():
    oidc.logout()
    return render_template("home.html")


@app.route('/can_be_submitted')
@oidc.require_login
def can_be_submitted():
    def parse_response(response):
        text = ast.literal_eval(response)
        return {"code": text["code"], "message": text["message"]}

    access_token = oidc.get_access_token()
    if not access_token:
        oidc.logout()
        return render_template("home.html")

    ref_number = request.args.get('ref_number')

    headers = {}
    headers['Authorization'] = f"Bearer {access_token}"
    url = REST_URL + "can_be_submitted?ref_number=" + ref_number
    r = requests.get(url, headers=headers)
    if r.status_code != 200:
        parsed_resp = parse_response(r.text)
        data = {"parsed_response": parsed_resp}
        return jsonify(data=data), r.status_code
    else:
        return jsonify(data={}), r.status_code


def extract_workflow_information():

    r = requests.get(REST_URL + "get_configuration")
    config_str = ast.literal_eval(r.text)["config"]
    with open("/app/config.json", "w") as fp:
        fp.write(config_str)

    with open("/app/config.json", "r") as fp:
        config = json.load(fp=fp)

    workflows = config["workflows"]
    input_parameters = {}
    for workflow_name in workflows:
        required = []
        optional = []
        for input_param in workflows[workflow_name]["input_parameters"]:
            filename, necessity = next(iter(input_param.items()))
            if necessity == 'required':
                required.append(filename)
            else:
                optional.append(filename)
        data = {"required": required, "optional": optional}
        input_parameters[workflow_name] = data

    return input_parameters


if __name__ == '__main__':
    app.run()
